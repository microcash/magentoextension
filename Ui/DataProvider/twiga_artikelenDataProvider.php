<?php
namespace Svenkalkman\Microcashapi\Ui\DataProvider;

use SoapClient;
//use function Svenkalkman\Microcashapi\Api\DeriveHash;
//use function Svenkalkman\Microcashapi\Api\GUID;
use Magento\Ui\DataProvider\AbstractDataProvider;
use Magento\Framework\App\Request\Http;
use Magento\Framework\App\Request\DataPersistorInterface;

class twiga_artikelenDataProvider extends AbstractDataProvider
{
    /**
     * @var DataPersistorInterface
     */
    protected $dataPersistor;

    /** @var CollectionFactory */
    protected $collection;

    /**
     * @var array
     */
    protected $loadedData;

    /**
     * @var \Magento\Ui\DataProvider\AddFieldToCollectionInterface[]
     */
    protected $addFieldStrategies;

    /**
     * @var \Magento\Ui\DataProvider\AddFilterToCollectionInterface[]
     */
    protected $addFilterStrategies;

    /**
     * @var \Magento\Framework\App\Request\Http
     */
    protected $request;

    protected $cleanarray;

    /**
     * Construct
     *
     * @param string $name
     * @param string $primaryFieldName
     * @param string $requestFieldName
     * @param array $meta
     * @param array $data
     */
    public function __construct(
        $name,
        $primaryFieldName,
        $requestFieldName,
        Http $request,
        \Svenkalkman\Microcashapi\Model\ResourceModel\artikelen_twiga\CollectionFactory $artikelen_twigaFactory,
        DataPersistorInterface $dataPersistor,
        array $meta = [],
        array $data = []
    ) {
        parent::__construct($name, $primaryFieldName, $requestFieldName, $meta, $data);
        $this->collection = $artikelen_twigaFactory->create();
        $this->dataPersistor = $dataPersistor;
        $this->request = $request;
    }

    /*public function GUID()
    {
        mt_srand((double)microtime()*10000);
        $charid = strtoupper(md5(uniqid(rand(), true)));
        $hyphen = chr(45);// "-"
        $uuid = chr(123)// "{"
            .substr($charid, 0, 8).$hyphen
            .substr($charid, 8, 4).$hyphen
            .substr($charid,12, 4).$hyphen
            .substr($charid,16, 4).$hyphen
            .substr($charid,20,12)
            .chr(125);// "}"
        return trim($uuid, '{}');
    }

    function DeriveHash($username, $secretkey, $requestid, $lang = 1)
    {
        $data = utf8_encode(strtoupper($username.$secretkey.strval($lang).$requestid));
        $hash = base64_encode(hash('sha256', $data, true ));
        return $hash;
    }*/

    public function getData()
    {
        /*if (isset($this->_loadedData)) {
            return $this->_loadedData;
        }
        $items = $this->collection->getItems();*/

        $items = $this->getCollection();

        $itemsarray = $items->toArray();


        return $itemsarray;


        /*foreach ($items as $artikel) {
            $this->_loadedData[$artikel->getId()] = $artikel->getData();
        }
        return $this->_loadedData;*/
        //echo "<script>console.log('" . json_encode(array_values($itemsarray)) . "');</script>";

        /*$username = 'mctest';
        $request = $this->GUID();
        $filiaal = 2;
        $key = '58CFA851-3A18-472B-91BC-FA24A49E5EEE';

        $header = [];
        $header['Language'] = 1;
        $header['Username'] = $username;
        $header['Filiaal'] = $filiaal;
        $header['RequestID'] = $request;
        $header['Hash'] = $this->DeriveHash($username, $key, $request, '1');

        $header2 = [];
        $header2['Versie'] = '0';

        $params = [];
        $params['pHeader'] = $header;
        $params['pArtikelenRequest'] = $header2;

        $options = array(
            'location' => 'http://sven_magento_devel.twigacloud1.microcash.nl/API/Webshop/Versie1',
            'uri' => 'http://sven_magento_devel.twigacloud1.microcash.nl/API/Webshop/Versie1',
            'trace' => true,
            'keep_alive' => false,
            'connection_timeout' => 10,
            'cache_wsdl' => WSDL_CACHE_NONE,
            'exceptions' => true,
        );

        $soapClient = new SoapClient('http://sven_magento_devel.twigacloud1.microcash.nl/API/Webshop/Versie1?singleWsdl', $options);

        $response = $soapClient->GetArtikelen($params);

        $array = $response->GetArtikelenResult->Artikelen->TwigaArtikel;

        $this->cleanarray = array();*/

        /*foreach($array as $artikel)
        {
            $requestid2 = $this->GUID();
            $newheader2 = [];
            $newheader2['Language'] = 1;
            $newheader2['Username'] = $username;
            $newheader2['Filiaal'] = $filiaal;
            $newheader2['RequestID'] = $requestid2;
            $newheader2['Hash'] = $this->DeriveHash($username, $key, $requestid2, '1');

            $newheader = [];
            $newheader['Barcode'] = $artikel->Hoofdbarcode;
            $newheader['ProductID'] = $artikel->StamID;
            $newparams = [];
            $newparams['pHeader'] = $newheader2;
            $newparams['pVoorraadRequest'] = $newheader;
            $newresponse = $soapClient->GetArtikelVoorraad($newparams);


            $artikel = $array[$i];
            $cleanarray[] = $artikel->StamID;
            $this->cleanarray[] = ['StamID' => $artikel->StamID, 'Omschrijving' => $artikel->Omschrijving, 'Prijs' => $artikel->Prijs, 'Leverancier' => $artikel->Leveranciers->TwigaLeverancier->Naam, 'Voorraad' => $artikel->Voorraad];
        }*/

        /*$items = $this->cleanarray;*/
        //var_dump($items);
        //echo "<script>console.log('" . json_encode("test") . "');</script>";
        /*
        $items = [
            ['filename_id' => "1", 'other_val' => 'dddd'],
            ['filename_id' => "2", 'other_val' => 'dddd'],
            ['filename_id' => "3", 'other_val' => 'dddd'],
            ['filename_id' => "4", 'other_val' => 'dddd'],
            ['filename_id' => "5", 'other_val' => 'dddd'],
            ['filename_id' => "6", 'other_val' => 'dddd'],
            ['filename_id' => "7", 'other_val' => 'dddd'],
            ['filename_id' => "8", 'other_val' => 'dddd'],
            ['filename_id' => "9", 'other_val' => 'dddd'],
            ['filename_id' => "10", 'other_val' => 'dddd'],
            ['filename_id' => "11", 'other_val' => 'dddd'],
            ['filename_id' => "12", 'other_val' => 'dddd'],
            ['filename_id' => "13", 'other_val' => 'dddd'],
            ['filename_id' => "14", 'other_val' => 'dddd'],
            ['filename_id' => "15", 'other_val' => 'dddd'],
            ['filename_id' => "16", 'other_val' => 'dddd'],
            ['filename_id' => "17", 'other_val' => 'dddd'],
            ['filename_id' => "18", 'other_val' => 'dddd'],
            ['filename_id' => "19", 'other_val' => 'dddd'],
            ['filename_id' => "20", 'other_val' => 'dddd'],
            ['filename_id' => "21", 'other_val' => 'dddd'],
            ['filename_id' => "22", 'other_val' => 'dddd'],
            ['filename_id' => "23", 'other_val' => 'dddd'],
            ['filename_id' => "24", 'other_val' => 'dddd'],
            ['filename_id' => "25", 'other_val' => 'dddd'],
            ['filename_id' => "26", 'other_val' => 'dddd'],
            ['filename_id' => "27", 'other_val' => 'dddd'],
        ];*/

        /*$pagesize = intval($this->request->getParam('listing_paging')['pageSize']);
        $pageCurrent = intval($this->request->getParam('listing_paging')['current']);
        $pageoffset = ($pageCurrent - 1)*$pagesize;*/

        //return array_slice($itemsarray,$pageoffset , $pageoffset+$pagesize);

        /*return [
            'totalRecords' => count($itemsarray),
            'items' => array_slice($itemsarray,$pageoffset , $pageoffset+$pagesize)
        ];*/
    }

    // ###########################################

    public function setLimit($offset, $size)
    {
    }

    public function addOrder($field, $direction)
    {
    }

    public function addFilter(\Magento\Framework\Api\Filter $filter)
    {
    }

    public function addField($field, $alias = null)
    {
    }

    public function GetArtikelen()
    {
        return $this->cleanarray;
    }

    /*public function getData()
    {
        $username = 'mctest';
        $request = GUID();
        $filiaal = 2;
        $key = '58CFA851-3A18-472B-91BC-FA24A49E5EEE';

        $header = [];
        $header['Language'] = 1;
        $header['Username'] = $username;
        $header['Filiaal'] = $filiaal;
        $header['RequestID'] = $request;
        $header['Hash'] = DeriveHash($username, $key, $request, '1');

        $header2 = [];
        $header2['Versie'] = '0';

        $params = [];
        $params['pHeader'] = $header;
        $params['pArtikelenRequest'] = $header2;

        $options = array(
            'location' => 'http://sven_magento_devel.twigacloud1.microcash.nl/API/Webshop/Versie1',
            'uri' => 'http://sven_magento_devel.twigacloud1.microcash.nl/API/Webshop/Versie1',
            'trace' => true,
            'keep_alive' => false,
            'connection_timeout' => 10,
            'cache_wsdl' => WSDL_CACHE_NONE,
            'exceptions' => true,
        );

        $soapClient = new SoapClient('http://sven_magento_devel.twigacloud1.microcash.nl/API/Webshop/Versie1?singleWsdl', $options);

        $response = $soapClient->GetArtikelen($params);

        $array = $response->GetArtikelenResult->Artikelen->TwigaArtikel;

        $cleanarray = array();

        for ($i = 1; $i <= 3; $i++)
        {
            $artikel = $array[$i];
            $cleanarray[] = $artikel->StamID;
        }

        return $cleanarray;
    }*/

}
?>










