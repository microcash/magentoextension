<?php
namespace Svenkalkman\Microcashapi\view\adminhtml\ui_component;

use Magento\Framework\Controller\ResultFactory;
use Magento\Backend\App\Action\Context;
use Magento\Ui\Component\MassAction\Filter;
use Svenkalkman\Microcashapi\Model\ResourceModel\artikelen_twiga\CollectionFactory;

class AddToCatalog extends \Magento\Backend\App\Action
{
    /**
     * Mass action Filter
     *
     * @var \Magento\Ui\Component\MassAction\Filter
     */
    protected $filter;

    /**
     * Collection Factory
     *
     * @var Vendor\Module\Model\ResourceModel\artikelen_twiga\CollectionFactory
     */
    protected $collectionFactory;


    public function __construct(Context $context, Filter $filter, CollectionFactory $collectionFactory)
    {
        $this->filter = $filter;
        $this->collectionFactory = $collectionFactory;
        parent::__construct($context);
    }

    public function execute()
    {
        echo "<script>console.log('" . json_encode("test") . "');</script>";
        $collection = $this->filter->getCollection($this->collectionFactory->create());
        $collectionSize = $collection->getSize();
        //var_dump($collection);
        echo "<script>console.log('" . json_encode("test") . "');</script>";
        foreach ($collection as $item){
            var_dump($item);
            $item['voorraad'] = 1;
        }

        $this->messageManager->addSuccessMessage(__('A total of %1 record(s) have been added.', $collectionSize));
        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        return $resultRedirect->setPath('*/*/');
    }

}

?>










