<?php

namespace Svenkalkman\Microcashapi\Model;


class artikelen_twiga extends \Magento\Framework\Model\AbstractModel implements \Magento\Framework\DataObject\IdentityInterface
{
    const CACHE_TAG = 'artikelen_twiga';

    protected $_cacheTag = 'artikelen_twiga';

    protected $_eventPrefix = 'artikelen_twiga';

    protected function _construct()
    {
        $this->_init('Svenkalkman\Microcashapi\Model\ResourceModel\artikelen_twiga');
    }

    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }

    public function getDefaultValues()
    {
        $values = [];

        return $values;
    }
}










