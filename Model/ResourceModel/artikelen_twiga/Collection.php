<?php

namespace Svenkalkman\Microcashapi\Model\ResourceModel\artikelen_twiga;


class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    protected $_idFieldName = 'id';
    protected $_eventPrefix = 'artikelen_twiga_collection';
    protected $_eventObject = 'artikelen_twiga_collection';

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Svenkalkman\Microcashapi\Model\artikelen_twiga', 'Svenkalkman\Microcashapi\Model\ResourceModel\artikelen_twiga');
    }
}










