<?php
namespace Svenkalkman\Microcashapi\Setup;

use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use SoapClient;

class InstallData implements InstallDataInterface
{
    protected $cleanarray;

    public function GUID()
    {
        mt_srand((double)microtime()*10000);
        $charid = strtoupper(md5(uniqid(rand(), true)));
        $hyphen = chr(45);// "-"
        $uuid = chr(123)// "{"
            .substr($charid, 0, 8).$hyphen
            .substr($charid, 8, 4).$hyphen
            .substr($charid,12, 4).$hyphen
            .substr($charid,16, 4).$hyphen
            .substr($charid,20,12)
            .chr(125);// "}"
        return trim($uuid, '{}');
    }

    function DeriveHash($username, $secretkey, $requestid, $lang = 1)
    {
        $data = utf8_encode(strtoupper($username.$secretkey.strval($lang).$requestid));
        $hash = base64_encode(hash('sha256', $data, true ));
        return $hash;
    }

    public function getData()
    {
        $username = 'mctest';
        $request = $this->GUID();
        $filiaal = 2;
        $key = '58CFA851-3A18-472B-91BC-FA24A49E5EEE';

        $header = [];
        $header['Language'] = 1;
        $header['Username'] = $username;
        $header['Filiaal'] = $filiaal;
        $header['RequestID'] = $request;
        $header['Hash'] = $this->DeriveHash($username, $key, $request, '1');

        $header2 = [];
        $header2['Versie'] = '0';

        $params = [];
        $params['pHeader'] = $header;
        $params['pArtikelenRequest'] = $header2;

        $options = array(
            'location' => 'http://sven_magento_devel.twigacloud1.microcash.nl/API/Webshop/Versie1',
            'uri' => 'http://sven_magento_devel.twigacloud1.microcash.nl/API/Webshop/Versie1',
            'trace' => true,
            'keep_alive' => false,
            'connection_timeout' => 10,
            'cache_wsdl' => WSDL_CACHE_NONE,
            'exceptions' => true,
        );

        $soapClient = new SoapClient('http://sven_magento_devel.twigacloud1.microcash.nl/API/Webshop/Versie1?singleWsdl', $options);

        $response = $soapClient->GetArtikelen($params);

        $array = $response->GetArtikelenResult->Artikelen->TwigaArtikel;

        $this->cleanarray = array();

        foreach($array as $artikel)
        {
            $this->cleanarray[] = ['id' => $artikel->StamID, 'omschrijving' => $artikel->Omschrijving, 'prijs' => $artikel->Prijs, "hoofdbarcode" => $artikel->Hoofdbarcode, 'voorraad' => $artikel->Voorraad, 'leverancier' => $artikel->Leveranciers->TwigaLeverancier->Naam];
        }

        $items = $this->cleanarray;

        return [
            'totalRecords' => count($items),
            'items' => array_slice($items)
        ];
    }

    public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        /**
         * Install messages
         */
        $data = $this->cleanarray;
        foreach ($data as $bind) {
            $setup->getConnection()
                ->insertForce($setup->getTable('artikelen_twiga'), $bind);
        }
    }

}

?>










