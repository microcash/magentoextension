<?php
namespace Svenkalkman\Microcashapi\Setup;

class InstallSchema implements \Magento\Framework\Setup\InstallSchemaInterface
{
    public function install(\Magento\Framework\Setup\SchemaSetupInterface $setup, \Magento\Framework\Setup\ModuleContextInterface $context)
    {
        $installer = $setup;
        $installer->startSetup();
        if (!$installer->tableExists('artikelen_twiga')) {
            $table = $installer->getConnection()->newTable(
                $installer->getTable('artikelen_twiga')
            )
                ->addColumn(
                    'id',
                    \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                    null,
                    [
                        'identity' => true,
                        'nullable' => false,
                        'primary'  => true,
                        'unsigned' => true,
                    ],
                    'ID'
                )
                ->addColumn(
                    'omschrijving',
                    \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    255,
                    ['nullable => false'],
                    'Omschrijving'
                )
                ->addColumn(
                    'prijs',
                    \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    255,
                    [],
                    'Prijs'
                )
                ->addColumn(
                    'hoofdbarcode',
                    \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    255,
                    [],
                    'Hoofdbarcode'
                )
                ->addColumn(
                    'voorraad',
                    \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                    1,
                    [],
                    'Voorraad'
                )
                ->addColumn(
                    'leverancier',
                    \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    255,
                    [],
                    'Leverancier'
                )
                ->setComment('Artikelen Table');
            $installer->getConnection()->createTable($table);

            $installer->getConnection()->addIndex(
                $installer->getTable('artikelen_twiga'),
                $setup->getIdxName(
                    $installer->getTable('artikelen_twiga'),
                    ['omschrijving','prijs','hoofdbarcode','leverancier'],
                    \Magento\Framework\DB\Adapter\AdapterInterface::INDEX_TYPE_FULLTEXT
                ),
                ['omschrijving','prijs','hoofdbarcode','leverancier'],
                \Magento\Framework\DB\Adapter\AdapterInterface::INDEX_TYPE_FULLTEXT
            );
        }
        $installer->endSetup();
    }

}

?>










